package spark.Utils;

import com.mongodb.*;
import com.mongodb.util.JSON;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import spark.models.Company;
import spark.models.Employee;
import spark.models.Report;

import java.lang.reflect.Array;
import java.net.UnknownHostException;
import java.util.*;

import static spark.Utils.DateHelper.*;

public class MongoHelper extends Mongo {

    private MongoClientURI uri = new MongoClientURI("mongodb://heroku_vvct1mpw:66teqaql94urq49valc74df38o@ds011492.mlab.com:11492/heroku_vvct1mpw");
    private MongoClient client;
    private DB db;
    // Connect: mongo ds011492.mlab.com:11492/heroku_vvct1mpw -u heroku_vvct1mpw -p 66teqaql94urq49valc74df38o
    // Total km: db.User.aggregate([{$group:{_id:"id",total: {$sum: "$totalDistance"}}}])
    // Total rides: db.User.aggregate([{$group:{_id:"id",total: {$sum: {$size:"$rides"}}}}])

    private static MongoHelper mongoH = null;

    private MongoHelper () throws UnknownHostException {
        client = new MongoClient(uri);
        db = client.getDB(uri.getDatabase());
    }

    /* Static 'instance' method */
    public static synchronized MongoHelper getInstance() throws UnknownHostException {
        if (mongoH == null){
            mongoH =  new MongoHelper();
        }
        return mongoH;
    }

    public boolean authCompany(String email, String password) {
        DBCollection company = db.getCollection("Company");
        BasicDBObject findQuery = new BasicDBObject("email", email);
        DBObject companyObject = company.findOne(findQuery);
        if (companyObject != null) {
            DBObject pass = (DBObject) companyObject.get("password");
            if (pass != null && Passwords.isExpectedPassword(password.toCharArray(), Base64.decodeBase64((String) pass.get("salt")), Base64.decodeBase64((String) pass.get("hash")))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void updateCompanyInfo(String email, BasicDBObject companyData) {
        DBCollection company = db.getCollection("Company");
        BasicDBObject updateQuery = new BasicDBObject();
        updateQuery.put("email", email);
        BasicDBObject updateCommand = new BasicDBObject();
        updateCommand.put("$set", companyData);
        company.update(updateQuery, updateCommand, true, true);
    }

    public void updateCompanyAddress(String email, BasicDBObject addressData) {
        DBCollection company = db.getCollection("Company");
        BasicDBObject updateQuery = new BasicDBObject();
        updateQuery.put("email", email);
        BasicDBObject updateCommand = new BasicDBObject();
        updateCommand.put("$addToSet", addressData);
        company.update(updateQuery, updateCommand, true, true);
    }

    public boolean isSessionActive(String email) {

        DBCollection company = db.getCollection("Company");
        BasicDBObject findQuery = new BasicDBObject("email", email);

        DBCursor cursor = company.find(findQuery);
        boolean sessionValid = false;

        try {
            if (cursor.hasNext()) {
                sessionValid = true;
            }
        } finally {
            cursor.close();
        }
        return sessionValid;
    }

    private boolean userExists(String userId) {
        DBCollection user = db.getCollection("User");
        BasicDBObject findQuery = new BasicDBObject("publicId", userId);

        DBCursor cursor = user.find(findQuery);
        boolean userExists = false;

        try {
            if (cursor.hasNext()) {
                userExists = true;
            }
        } finally {
            cursor.close();
        }
        return userExists;
    }

    public boolean companyExists(String email) {
        DBCollection company = db.getCollection("Company");
        BasicDBObject findQuery = new BasicDBObject("email", email);

        DBCursor cursor = company.find(findQuery);
        boolean companyExists = false;

        try {
            if (cursor.hasNext()) {
                companyExists = true;
            }
        } finally {
            cursor.close();
        }
        return companyExists;
    }

    public boolean saveUser(String jsonUser) {
        DBCollection users = db.getCollection("User");
        DBObject dbObject = (DBObject) JSON.parse(jsonUser);
        if (userExists((String) dbObject.get("publicId"))) {
            return false;
        }
        users.insert(dbObject);
        return true;
    }

    public JSONObject getUserBasic (String userId) {

        DBCollection users = db.getCollection("User");

        BasicDBObject query = new BasicDBObject("publicId", userId);
        BasicDBObject constrains = new BasicDBObject().append("domain", 1).append("totalDistance", 1).append("_id", 0);

        DBObject userObject = users.findOne(query, constrains);

        if (userObject != null) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("domain", userObject.get("domain"));
            jsonObject.put("totalDistance", userObject.get("totalDistance"));
            return jsonObject;
        } else {
            return null;
        }
    }

    public void saveCompany(BasicDBObject companyData, String email, BasicDBObject addressData) {
        DBCollection companies = db.getCollection("Company");
        companies.insert(companyData);
        mongoH.updateCompanyAddress(email, addressData);
    }

    public boolean updateWorkEmail(String userId, String jsonEmail) {
        DBCollection users = db.getCollection("User");
        BasicDBObject updateQuery = new BasicDBObject();
        updateQuery.put("publicId", userId);

        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonEmail);
            String workEmail = (String) jsonObject.get("workEmail");
            BasicDBObject updateCommand = new BasicDBObject();
            BasicDBObject updateFields = new BasicDBObject();
            updateFields.put("workEmail", workEmail);
            updateFields.put("domain", workEmail.substring(workEmail.lastIndexOf("@")));
            updateCommand.put("$set", updateFields);
            WriteResult result = users.update(updateQuery, updateCommand, true, true);

        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public boolean saveRides(String userId, String jsonRides) {
        DBCollection users = db.getCollection("User");
        BasicDBObject updateQuery = new BasicDBObject("publicId", userId);
        JSONParser jsonParser = new JSONParser();
        try {

            JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonRides);
            JSONArray ridesArray = (JSONArray) jsonObject.get("rides");
            Iterator i = ridesArray.iterator();
            while (i.hasNext()) {

                JSONObject ride = (JSONObject) i.next();
                double rideDistance = Double.parseDouble((String) ride.get("distance"));
                String rideDate = (String) ride.get("date");
                double totalDistance = getTotalDistance(userId);
                double monthDistance = getTimeDistance(userId, Constants.MONTH, rideDate, true);
                double weekDistance = getTimeDistance(userId, Constants.WEEK, rideDate, true);
                double dayDistance = getTimeDistance(userId, Constants.DAY, rideDate, true);

                BasicDBObject monthDistanceObject = new BasicDBObject("month", dateToTimeString(Constants.MONTH, rideDate));
                monthDistanceObject.append("distance", monthDistance + rideDistance);
                BasicDBObject weekDistanceObject = new BasicDBObject("week", dateToTimeString(Constants.WEEK, rideDate));
                weekDistanceObject.append("distance", weekDistance + rideDistance);
                BasicDBObject dayDistanceObject = new BasicDBObject("day", dateToTimeString(Constants.DAY, rideDate));
                dayDistanceObject.append("distance", dayDistance + rideDistance);

                BasicDBObject updateCommand = new BasicDBObject();
                BasicDBObject pushArrayObjects = new BasicDBObject().append("monthDistance", monthDistanceObject)
                        .append("weekDistance", weekDistanceObject)
                        .append("dayDistance", dayDistanceObject);
                updateCommand.put("$push", pushArrayObjects);
                updateCommand.put("$set", new BasicDBObject("totalDistance", totalDistance + rideDistance));
                updateCommand.put("$addToSet", new BasicDBObject("rides", ride));

                WriteResult result = users.update(updateQuery, updateCommand, true, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private double getTotalDistance(String userId) {
        DBCollection users = db.getCollection("User");
        BasicDBObject updateQuery = new BasicDBObject("publicId", userId);
        DBObject userObject = users.findOne(updateQuery);
        if (userObject != null) {
            return ((Number) userObject.get("totalDistance")).doubleValue();
        }
        return 0.0;
    }

    private String getTimeString(int timeType){
        switch (timeType){
            case Constants.DAY:
                return "day";
            case Constants.WEEK:
                return "week";
            case Constants.MONTH:
                return "month";
            default:
                return "";
        }
    }

    private double getTimeDistance(String userId, int timeType, String stringDate, boolean pullFromDb) {
        String type = getTimeString(timeType);
        String typeDistance = type + "Distance";

        DBCollection users = db.getCollection("User");
        String timeString = dateToTimeString(timeType, stringDate);
        BasicDBObject query = new BasicDBObject("publicId", userId);
        BasicDBObject condition = new BasicDBObject(type, timeString);
        BasicDBObject elemMatch = new BasicDBObject("$elemMatch", condition);
        BasicDBObject fields = new BasicDBObject().append("_id", 0).append(typeDistance, elemMatch);

        DBCursor cursor = users.find(query, fields);
        try {
            if (cursor.hasNext()) {
                BasicDBObject cursorObject = (BasicDBObject) cursor.next();
                if (!cursorObject.isEmpty()) {
                    BasicDBList timeDistance = (BasicDBList) cursorObject.get(typeDistance);
                    if (!timeDistance.isEmpty()) {
                        BasicDBObject timeDistanceObject = (BasicDBObject) timeDistance.get(0);
                        if (timeDistanceObject != null) {
                            if (pullFromDb) {
                                pullTimeDistance(userId, timeType, stringDate);
                            }
                            return ((Number) timeDistanceObject.get("distance")).doubleValue();
                        }
                    }
                }
            }
        } finally {
            cursor.close();
        }
        return 0.0;
    }

    private void pullTimeDistance(String userId, int timeType, String stringDate){
        String type = getTimeString(timeType);
        String typeDistance = type + "Distance";

        DBCollection users = db.getCollection("User");
        BasicDBObject query = new BasicDBObject("publicId", userId);

        BasicDBObject updateCommand = new BasicDBObject();
        BasicDBObject condition = new BasicDBObject(type, dateToTimeString(timeType, stringDate));
        BasicDBObject pullArrayObject = new BasicDBObject(typeDistance, condition);
        updateCommand.put("$pull", pullArrayObject);
        users.update(query, updateCommand, true, true);
    }

    public boolean hasValidLocation(String email) {
        DBCollection company = db.getCollection("Company");
        BasicDBObject findQuery = new BasicDBObject("email", email);
        DBCursor cursor = company.find(findQuery);
        try {
            if (cursor.hasNext()) {
                DBObject doc = cursor.next();

                BasicDBList locations = (BasicDBList) doc.get("locations");

                BasicDBObject location = (BasicDBObject) locations.get(0);

                String lat = (String) location.get("latitude");
                String lon = (String) location.get("longitude");
                if (lat != null && lon != null && !lat.equals("") && !lon.equals("")) {
                    return true;
                }
            }
        } finally {
            cursor.close();
        }
        return false;
    }

    private BasicDBObject getCompanySimple(String email) {
        DBCollection company = db.getCollection("Company");
        BasicDBObject findQuery = new BasicDBObject("email", email);
        BasicDBObject companyData = new BasicDBObject();
        ArrayList<BasicDBObject> companyLocation = new ArrayList<BasicDBObject>();

        DBCursor cursor = company.find(findQuery);
        try {
            if (cursor.hasNext()) {
                DBObject doc = cursor.next();

                companyData.put("domain", doc.get("domain"));
                companyData.put("startDate", doc.get("startDate"));
                companyData.put("budget", doc.get("budget"));

                BasicDBList locations = (BasicDBList) doc.get("locations");

                for(Object dbObj : locations) {
                    // shows each item from the locations array
                    BasicDBObject location = (BasicDBObject) dbObj;
                    location.put("latitude", location.get("latitude"));
                    location.put("longitude", location.get("longitude"));
                    companyLocation.add(location);
                }

                companyData.put ("locations", companyLocation);
            }
        } finally {
            cursor.close();
        }
        return companyData;
    }

    public Company getCompanyFull(String email) {
        DBCollection company = db.getCollection("Company");
        BasicDBObject findQuery = new BasicDBObject("email", email);
        DBObject companyObject = company.findOne(findQuery);
        if (companyObject != null) {
            Company companyData = new Company((String) companyObject.get("name"));
            companyData.setEmail((String) companyObject.get("email"));
            companyData.setDomain((String) companyObject.get("domain"));
            companyData.setDate((String) companyObject.get("startDate"));
            companyData.setBudget((String) companyObject.get("budget"));
            BasicDBList locations = (BasicDBList) companyObject.get("locations");
            BasicDBObject location = (BasicDBObject) locations.get(0);
            companyData.setStreet((String) location.get("street"));
            companyData.setNumber(Integer.parseInt((String) location.get("number")));
            companyData.setCity((String) location.get("city"));
            companyData.setCountry((String) location.get("country"));
            companyData.setCode((String) location.get("zipCode"));
            return companyData;
        } else {
            return null;
        }
    }


    private ArrayList<DBObject> getEmployeesData(String domain) {
        DBCollection company = db.getCollection("User");
        BasicDBObject findQuery = new BasicDBObject("domain", domain);

        DBCursor cursor = company.find(findQuery);
        ArrayList<DBObject> employeeData = new ArrayList<DBObject>();

        try {
            while (cursor.hasNext()) {
                DBObject doc = cursor.next();
                employeeData.add(doc);
            }
        } finally {
            cursor.close();
        }
        return employeeData;
    }

    public Report getReport(String email) {
        BasicDBObject companyData = getCompanySimple(email);

        String domain = (String) companyData.get("domain");
        String budgetString = (String) companyData.get("budget");
        String startDate = getStartDate((String) companyData.get("startDate"));
        ArrayList<BasicDBObject> locations = (ArrayList<BasicDBObject>) companyData.get("locations");
        ArrayList<DBObject> rawUserData = getEmployeesData(domain);
        ArrayList<Employee> employeesList = new ArrayList<>();
        for (DBObject object : rawUserData) {
            Employee employee = new Employee((String) object.get("name"));
            employee.setEmail((String) object.get("workEmail"));
            BasicDBList rides = (BasicDBList) object.get("rides");
            Iterator i = rides.iterator();
            while (i.hasNext()) {
                BasicDBObject ride = (BasicDBObject) i.next();
                String rideDate = (String) ride.get("date");
                if (belongsToReport(rideDate, startDate)) {
                    double startLat = Double.parseDouble((String) ride.get("startLatitude"));
                    double startLon = Double.parseDouble((String) ride.get("startLongitude"));
                    double endLat = Double.parseDouble((String) ride.get("endLatitude"));
                    double endLon = Double.parseDouble((String) ride.get("endLongitude"));
                    for (BasicDBObject loc : locations) {
                        double companyLat = Double.parseDouble((String) loc.get("latitude"));
                        double companyLon = Double.parseDouble((String) loc.get("longitude"));

                        if (Geocoder.distance(startLat, startLon, companyLat, companyLon) < 0.5
                                || Geocoder.distance(endLat, endLon, companyLat, companyLon) < 0.5) {
                            employee.setDistance(employee.getDistance() + Double.parseDouble((String) ride.get("distance")));
                            employee.setTime(employee.getTime() + Double.parseDouble((String) ride.get("time")));
                            employee.setRides(employee.getRides() + 1);
                            break;
                        }
                    }
                }
            }
            if (budgetString != null && !budgetString.equals("")) {
                double budget = Double.parseDouble(budgetString);
                employee.setEarnings(calculateEarnings(budget, employee.getDistance() / 1000));
            }
            employeesList.add(employee);
        }
        Collections.sort(employeesList, Comparator.comparing(Employee::getDistance));
        Collections.reverse(employeesList);
        Report report = new Report(stringToFormatString(startDate));
        report.setEmployeesData(employeesList);
        report.setEndDate(dateToFormatString(new Date()));
        report.setIsBudgetSet(budgetString != null && !budgetString.equals(""));
        return report;
    }

    public void saveTask() {
        DBCollection dates = db.getCollection("Dates");
        DBObject dateObject = new BasicDBObject("dateUpdated", new Date());
        dates.insert(dateObject);
    }

    public double calculateEarningsPerKm(double budget) {
        return (budget/ (Constants.AVG_COMMUTE_KM * 22));
    }

    private double calculateEarnings(double budget, double distance) {
        double earningsPerKm = calculateEarningsPerKm(budget);
        double earnings = earningsPerKm * distance;
        return earnings > budget ? budget : earnings;
    }

    private JSONObject getTotalLeaderboard (int version, double distance, String domain, boolean usersAbove){
        String comparation, jsonName;
        int order, numberOfUsers;
        if (usersAbove){
            comparation = "$gt";
            order = 1;
            if (version == 1) {
                numberOfUsers = Constants.USERS_ABOVE;
            } else {
                numberOfUsers = Constants.USERS_ABOVE_V2;
            }
            jsonName = "usersAbove";
        } else {
            comparation = "$lt";
            order = -1;
            if (version == 1) {
                numberOfUsers = Constants.USERS_BELOW;
            } else {
                numberOfUsers = Constants.USERS_BELOW_V2;
            }
            jsonName = "usersBelow";
        }
        DBCollection users = db.getCollection("User");
        BasicDBObject condition = new BasicDBObject(comparation, distance);
        BasicDBObject query = new BasicDBObject();
        if (domain.equals("")) {
            query.append("totalDistance", condition);
        } else {
            query.append("domain", domain).append("totalDistance", condition);
        }
        BasicDBObject excludeNotNeededFields = new BasicDBObject().append("totalDistance", 1).append("name", 1).append("_id", 0);
        BasicDBObject orderBy = new BasicDBObject("totalDistance", order);
        DBCursor cursor = users.find(query,excludeNotNeededFields).sort(orderBy);
        JSONObject leaderboard = new JSONObject();
        if (usersAbove) {
            leaderboard.put("position", cursor.count() + 1);
        }
        JSONArray usersAround = new JSONArray();
        int count = 0;
        while (cursor.hasNext() && count < numberOfUsers){
            DBObject userObject = cursor.next();
            JSONObject user = new JSONObject();
            user.put("name", userObject.get("name"));
            user.put("distance", userObject.get("totalDistance"));
            usersAround.add(user);
            count ++;
        }
        leaderboard.put(jsonName, usersAround);
        return leaderboard;
    }

    private JSONObject getTimeLeaderboard (int version, int timeType, double distance, String domain, boolean usersAbove){
        String comparation, jsonName;
        int order, numberOfUsers;
        if (usersAbove){
            comparation = "$gt";
            order = 1;
            if (version == 1) {
                numberOfUsers = Constants.USERS_ABOVE;
            } else {
                numberOfUsers = Constants.USERS_ABOVE_V2;
            }
            jsonName = "usersAbove";
        } else {
            comparation = "$lt";
            order = -1;
            if (version == 1) {
                numberOfUsers = Constants.USERS_BELOW;
            } else {
                numberOfUsers = Constants.USERS_BELOW_V2;
            }
            jsonName = "usersBelow";
        }
        String date = dateToTimeString(timeType, dateToString(new Date()));
        DBCollection users = db.getCollection("User");
        String type = getTimeString(timeType);
        String typeDistance = type + "Distance";
        BasicDBObject conditionGt = new BasicDBObject(comparation, distance);
        BasicDBObject condition = new BasicDBObject(type, date)
                .append("distance", conditionGt);
        BasicDBObject elemMatch = new BasicDBObject("$elemMatch", condition);
        BasicDBObject query = new BasicDBObject();
        if (domain.equals("")) {
            query.append(typeDistance, elemMatch);
        } else {
            query.append("domain", domain).append(typeDistance, elemMatch);
        }
        BasicDBObject excludeNotNeededFields = new BasicDBObject().append(typeDistance + ".distance", 1).append(typeDistance + "." + type, 1).append("name", 1).append("_id", 0);
        BasicDBObject orderBy = new BasicDBObject(typeDistance + "." + type, -1).append(typeDistance + ".distance", order);

        DBCursor cursor = users.find(query, excludeNotNeededFields).sort(orderBy);
        JSONObject leaderboard = new JSONObject();
        if (usersAbove) {
            leaderboard.put("position", cursor.count() + 1);
        }
        JSONArray usersAround = new JSONArray();
        int count = 0;
        while (cursor.hasNext() && count < numberOfUsers){
            DBObject userObject = cursor.next();
            JSONObject user = new JSONObject();
            user.put("name", userObject.get("name"));
            BasicDBList list = (BasicDBList) userObject.get(typeDistance);

            BasicDBObject obj = new BasicDBObject();
            for (Object listElement : list){
                BasicDBObject dbObj = (BasicDBObject) listElement;
                if (dbObj.get(type).equals(date)){
                    obj = dbObj;
                }
            }

            user.put("distance", obj.get("distance"));
            usersAround = addUserToArray(user, usersAround, usersAbove);
            count ++;
        }

        leaderboard.put(jsonName, usersAround);
        return leaderboard;
    }

    private JSONArray addUserToArray(JSONObject user, JSONArray usersAround, boolean usersAbove) {
        boolean userAdded = false;
        double distance = ((Number) user.get("distance")).doubleValue();
        int arraySize = usersAround.size();
        int i;
        double elementDistance;

        if (arraySize == 0){
            usersAround.add(user);
            userAdded = true;
        } else if (usersAbove){
            for (i = 0; i < arraySize; i ++){
                elementDistance = ((Number)((JSONObject)usersAround.get(i)).get("distance")).doubleValue();
                if (distance == elementDistance){
                    userAdded = true;
                    break;
                }
            }
            if (!userAdded) {
                for (i = 0; i < arraySize; i ++){
                    elementDistance = ((Number)((JSONObject)usersAround.get(i)).get("distance")).doubleValue();
                    if (distance < elementDistance){
                        usersAround.add(i, user);
                        userAdded = true;
                        break;
                    }
                }
            }

        } else {
            for (i = 0; i < arraySize; i ++){
                elementDistance = ((Number)((JSONObject)usersAround.get(i)).get("distance")).doubleValue();
               if (distance == elementDistance){
                    userAdded = true;
                   break;
                }
            }
            if (!userAdded){
                for (i = 0; i < arraySize; i ++) {
                    elementDistance = ((Number) ((JSONObject) usersAround.get(i)).get("distance")).doubleValue();
                    if (distance > elementDistance) {
                        usersAround.add(i, user);
                        userAdded = true;
                        break;
                    }
                }
            }
        }
        if (!userAdded) {
            usersAround.add(user);
        }
        return usersAround;
    }

    public JSONObject getLeaderboard(String userId, int version) {

        JSONObject userBasic = getUserBasic(userId);
        String domain = (String) userBasic.get("domain");

        JSONObject totalLeaderboard = new JSONObject();
        double totalDistance = ((Number) userBasic.get("totalDistance")).doubleValue();
        JSONObject aboveTotalLeaderboard = getTotalLeaderboard(version, totalDistance, "", true);

        JSONObject belowTotalLeaderboard = getTotalLeaderboard(version, totalDistance, "", false);
        totalLeaderboard.put("usersAbove", aboveTotalLeaderboard.get("usersAbove"));
        totalLeaderboard.put("position", aboveTotalLeaderboard.get("position"));
        totalLeaderboard.put("usersBelow", belowTotalLeaderboard.get("usersBelow"));
        totalLeaderboard.put("distance", totalDistance);

        JSONObject monthLeaderboard = new JSONObject();
        double monthDistance = getTimeDistance(userId, Constants.MONTH, dateToString(new Date()), false);
        JSONObject aboveMonthLeaderboard = getTimeLeaderboard(version, Constants.MONTH, monthDistance, "", true);
        JSONObject belowMonthLeaderboard = getTimeLeaderboard(version, Constants.MONTH, monthDistance, "", false);
        monthLeaderboard.put("usersAbove", aboveMonthLeaderboard.get("usersAbove"));
        monthLeaderboard.put("position", aboveMonthLeaderboard.get("position"));
        monthLeaderboard.put("usersBelow", belowMonthLeaderboard.get("usersBelow"));
        monthLeaderboard.put("distance", monthDistance);

        JSONObject weekLeaderboard = new JSONObject();
        double weekDistance = getTimeDistance(userId, Constants.WEEK, dateToString(new Date()), false);
        JSONObject aboveWeekLeaderboard = getTimeLeaderboard(version, Constants.WEEK, weekDistance, "", true);
        JSONObject belowWeekLeaderboard = getTimeLeaderboard(version, Constants.WEEK, weekDistance, "", false);
        weekLeaderboard.put("usersAbove", aboveWeekLeaderboard.get("usersAbove"));
        weekLeaderboard.put("position", aboveWeekLeaderboard.get("position"));
        weekLeaderboard.put("usersBelow", belowWeekLeaderboard.get("usersBelow"));
        weekLeaderboard.put("distance", weekDistance);

        JSONObject dayLeaderboard = new JSONObject();
        double dayDistance = getTimeDistance(userId, Constants.DAY, dateToString(new Date()), false);
        JSONObject aboveDayLeaderboard = getTimeLeaderboard(version, Constants.DAY, dayDistance, "", true);
        JSONObject belowDayLeaderboard = getTimeLeaderboard(version, Constants.DAY, dayDistance, "", false);
        dayLeaderboard.put("usersAbove", aboveDayLeaderboard.get("usersAbove"));
        dayLeaderboard.put("position", aboveDayLeaderboard.get("position"));
        dayLeaderboard.put("usersBelow", belowDayLeaderboard.get("usersBelow"));
        dayLeaderboard.put("distance", dayDistance);

        JSONObject leaderboard = new JSONObject();
        leaderboard.put("total", totalLeaderboard);
        leaderboard.put("month", monthLeaderboard);
        leaderboard.put("week", weekLeaderboard);
        leaderboard.put("day", dayLeaderboard);
        leaderboard.put("domain", domain);

        if (domain != null && !domain.equals("")){

            JSONObject totalCompanyLeaderboard = new JSONObject();
            JSONObject aboveTotalCompanyLeaderboard = getTotalLeaderboard(version, totalDistance, domain, true);
            JSONObject belowTotalCompanyLeaderboard = getTotalLeaderboard(version, totalDistance, domain, false);
            totalCompanyLeaderboard.put("usersAbove", aboveTotalCompanyLeaderboard.get("usersAbove"));
            totalCompanyLeaderboard.put("position", aboveTotalCompanyLeaderboard.get("position"));
            totalCompanyLeaderboard.put("usersBelow", belowTotalCompanyLeaderboard.get("usersBelow"));
            totalCompanyLeaderboard.put("distance", totalDistance);

            JSONObject monthCompanyLeaderboard = new JSONObject();
            JSONObject aboveMonthCompanyLeaderboard = getTimeLeaderboard(version, Constants.MONTH, monthDistance, domain, true);
            JSONObject belowMonthCompanyLeaderboard = getTimeLeaderboard(version, Constants.MONTH, monthDistance, domain, false);
            monthCompanyLeaderboard.put("usersAbove", aboveMonthCompanyLeaderboard.get("usersAbove"));
            monthCompanyLeaderboard.put("position", aboveMonthCompanyLeaderboard.get("position"));
            monthCompanyLeaderboard.put("usersBelow", belowMonthCompanyLeaderboard.get("usersBelow"));
            monthCompanyLeaderboard.put("distance", monthDistance);

            JSONObject weekCompanyLeaderboard = new JSONObject();
            JSONObject aboveWeekCompanyLeaderboard = getTimeLeaderboard(version, Constants.WEEK, weekDistance, domain, true);
            JSONObject belowWeekCompanyLeaderboard = getTimeLeaderboard(version, Constants.WEEK, weekDistance, domain, false);
            weekCompanyLeaderboard.put("usersAbove", aboveWeekCompanyLeaderboard.get("usersAbove"));
            weekCompanyLeaderboard.put("position", aboveWeekCompanyLeaderboard.get("position"));
            weekCompanyLeaderboard.put("usersBelow", belowWeekCompanyLeaderboard.get("usersBelow"));
            weekCompanyLeaderboard.put("distance", weekDistance);

            JSONObject dayCompanyLeaderboard = new JSONObject();
            JSONObject aboveDayCompanyLeaderboard = getTimeLeaderboard(version, Constants.DAY, dayDistance, domain, true);
            JSONObject belowDayCompanyLeaderboard = getTimeLeaderboard(version, Constants.DAY, dayDistance, domain, false);
            dayCompanyLeaderboard.put("usersAbove", aboveDayCompanyLeaderboard.get("usersAbove"));
            dayCompanyLeaderboard.put("position", aboveDayCompanyLeaderboard.get("position"));
            dayCompanyLeaderboard.put("usersBelow", belowDayCompanyLeaderboard.get("usersBelow"));
            dayCompanyLeaderboard.put("distance", dayDistance);

            leaderboard.put("totalCompany", totalCompanyLeaderboard);
            leaderboard.put("monthCompany", monthCompanyLeaderboard);
            leaderboard.put("weekCompany", weekCompanyLeaderboard);
            leaderboard.put("dayCompany", dayCompanyLeaderboard);
        }
        return leaderboard;
    }

    private JSONArray getGeneralLeaderboard(String domain) {

        DBCollection users = db.getCollection("User");
        String date = dateToTimeString(Constants.MONTH, dateToString(new Date()));

        BasicDBObject query = new BasicDBObject();
        BasicDBObject condition = new BasicDBObject("month", date);
        BasicDBObject elemMatch = new BasicDBObject("$elemMatch", condition);
        query.append("domain", domain).append("monthDistance", elemMatch);

        BasicDBObject excludeNotNeededFields = new BasicDBObject().append("monthDistance.distance", 1).append("monthDistance.month", 1).append("name", 1).append("_id", 0);
        BasicDBObject orderBy = new BasicDBObject("monthDistance.month", -1).append("monthDistance.distance", -1);
        DBCursor cursor = users.find(query, excludeNotNeededFields).sort(orderBy);

        JSONArray usersInGeneralLeaderboard = new JSONArray();
        while (cursor.hasNext()){
            DBObject userObject = cursor.next();
            JSONObject user = new JSONObject();
            user.put("name", userObject.get("name"));

            BasicDBList list = (BasicDBList) userObject.get("monthDistance");
            BasicDBObject obj = new BasicDBObject();
            for (Object listElement : list){
                BasicDBObject dbObj = (BasicDBObject) listElement;
                if (dbObj.get("month").equals(date)){
                    obj = dbObj;
                }
            }
            user.put("distance", obj.get("distance"));
            usersInGeneralLeaderboard.add(user);
        }

        return usersInGeneralLeaderboard;
    }

    public String getStringFormattedLeaderboard(String domain){
        String stringLeaderboard = "";
        int count = 0;
        JSONArray leaderboardArray = (JSONArray) getGeneralLeaderboard(domain);
        Iterator i = leaderboardArray.iterator();
        while (i.hasNext()) {
            count++;
            JSONObject leaderboardEntry = (JSONObject) i.next();

            String pos = Integer.toString(count);
            String name = (String) leaderboardEntry.get("name");
            String dist = formatDistance(((Number) leaderboardEntry.get("distance")).doubleValue());

            stringLeaderboard = stringLeaderboard + "*" + pos + ". " + name + ":* " + dist + "\n";
        }
        return stringLeaderboard.trim();
    }

    public JSONObject getGlobalVariables(){

        DBCollection variables = db.getCollection("Variables");

        DBObject variablesObject = variables.findOne();

        if (variablesObject != null) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("DIST_FILTER", variablesObject.get("DIST_FILTER"));
            jsonObject.put("MAX_VAL_ACC", variablesObject.get("MAX_VAL_ACC"));
            jsonObject.put("MAX_WALK_SPEED", variablesObject.get("MAX_WALK_SPEED"));
            jsonObject.put("MIN_HIGH_SPEED", variablesObject.get("MIN_HIGH_SPEED"));
            jsonObject.put("MIN_CAR_SPEED", variablesObject.get("MIN_CAR_SPEED"));
            jsonObject.put("MIN_BIKE_SPEED", variablesObject.get("MIN_BIKE_SPEED"));
            jsonObject.put("MAX_BIKE_SPEED", variablesObject.get("MAX_BIKE_SPEED"));
            jsonObject.put("MIN_BIKE_DIST_PERC", variablesObject.get("MIN_BIKE_DIST_PERC"));
            jsonObject.put("MIN_VAL_LOC", variablesObject.get("MIN_VAL_LOC"));
            jsonObject.put("PREV_TO_FIRST_DIST", variablesObject.get("PREV_TO_FIRST_DIST"));
            jsonObject.put("PERC_DIST_ADDED", variablesObject.get("PERC_DIST_ADDED"));
            jsonObject.put("MAX_E_BIKE_SPEED", variablesObject.get("MAX_E_BIKE_SPEED"));
            jsonObject.put("MIN_E_BIKE_DIST_PERC", variablesObject.get("MIN_E_BIKE_DIST_PERC"));
            jsonObject.put("EXTRA_CONFIG_1", variablesObject.get("EXTRA_CONFIG_1"));
            jsonObject.put("EXTRA_CONFIG_2", variablesObject.get("EXTRA_CONFIG_2"));
            jsonObject.put("EXTRA_CONFIG_3", variablesObject.get("EXTRA_CONFIG_3"));

            return jsonObject;
        } else {
            return null;
        }
    }

    private String formatDistance(double doubleDistance){
        String distance;
        if (doubleDistance < 1000){
            distance = String.format("%.0f", doubleDistance) + " m";
        } else {
            distance = String.format("%.2f", doubleDistance/1000) + " km";
        }
        return distance;
    }
}
