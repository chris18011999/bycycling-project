package spark.Utils;

/**
 * Created by gonzovilla89 on 06/06/16.
 */

import java.net.UnknownHostException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MINUTES;

public class BackgroundService {
    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public static void taskEveryXMinutes(int minutes) {
        final ScheduledFuture<?> taskHandle = scheduler.scheduleAtFixedRate(new Runnable() {
            public void run() {
                System.out.println("BACKGROUND TASK RUNNING");
                try {
                    MongoHelper mongoH = MongoHelper.getInstance();
                    //mongoH.saveTask();
                    String emailBody = mongoH.getStringFormattedLeaderboard("@newworldcampus.nl");
                    EmailHelper.sendLeaderboardsEmail(emailBody);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }, minutes, minutes, MINUTES);
    }

    public static void taskEveryXHoursAfter(int hours, int hoursAfter) {
        final ScheduledFuture<?> taskHandle = scheduler.scheduleAtFixedRate(new Runnable() {
            public void run() {
                System.out.println("BACKGROUND TASK RUNNING");
                try {
                    MongoHelper mongoH = MongoHelper.getInstance();
                    //mongoH.saveTask();
                    String emailBody = mongoH.getStringFormattedLeaderboard("@newworldcampus.nl");
                    EmailHelper.sendLeaderboardsEmail(emailBody);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }, hoursAfter, hours, HOURS);
    }
}
